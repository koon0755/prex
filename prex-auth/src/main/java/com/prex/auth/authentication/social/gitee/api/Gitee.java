package com.prex.auth.authentication.social.gitee.api;

/**
 * @Classname Gitee
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-07-08 22:04
 * @Version 1.0
 */
public interface Gitee {

    /**
     * 获取用户信息
     *
     * @return
     */
    GiteeUserInfo getUserInfo();
}
