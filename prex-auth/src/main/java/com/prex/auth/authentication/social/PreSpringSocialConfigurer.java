package com.prex.auth.authentication.social;

import com.prex.auth.handler.PrexAuthenticationSuccessHandler;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.security.SocialAuthenticationFilter;
import org.springframework.social.security.SpringSocialConfigurer;

/**
 * @Classname GiteeAdapter
 * @Description
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-07-08 21:49
 * @Version 1.0
 */
@Data
public class PreSpringSocialConfigurer extends SpringSocialConfigurer {
	
	private String filterProcessesUrl;

	@Autowired
	private PrexAuthenticationSuccessHandler preAuthenticationSuccessHandler;

	private SocialAuthenticationFilterPostProcessor socialAuthenticationFilterPostProcessor;

	public PreSpringSocialConfigurer(String filterProcessesUrl) {
		this.filterProcessesUrl = filterProcessesUrl;
	}
	
	@Override
	protected <T> T postProcess(T object) {
		SocialAuthenticationFilter filter = (SocialAuthenticationFilter) super.postProcess(object);

		filter.setFilterProcessesUrl(filterProcessesUrl);

		if(socialAuthenticationFilterPostProcessor != null){
			socialAuthenticationFilterPostProcessor.process(filter);
		}
		filter.setSignupUrl("/socialSignUp");
//		filter.setAuthenticationSuccessHandler(preAuthenticationSuccessHandler);
		return (T) filter;
	}

}
