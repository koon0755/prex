package com.prex.client;

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

/**
 * @ClassName PrexClientApplication
 * @Description TODO
 * @Author yanlin
 * @Version v1.0
 * @Date 2019-08-20 5:34 PM
 **/
@EnableDiscoveryClient
@SpringBootApplication
public class PrexClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(PrexClientApplication.class, args);
    }

    @Bean
    public SentinelResourceAspect sentinelResourceAspect() {
        return new SentinelResourceAspect();
    }
}
