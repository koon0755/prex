package com.prex.base.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prex.base.api.entity.SysOauthClientDetails;
import com.prex.base.server.mapper.SysOauthClientDetailsMapper;
import com.prex.base.server.service.SysOauthClientDetailsService;
import org.springframework.stereotype.Service;

/**
 * @Classname SysOauthClientDetailsServiceImpl
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-05 11:42
 * @Version 1.0
 */
@Service
public class SysOauthClientDetailsServiceImpl extends ServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails> implements SysOauthClientDetailsService {

}
