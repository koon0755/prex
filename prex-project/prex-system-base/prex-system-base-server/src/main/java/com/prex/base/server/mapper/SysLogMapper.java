package com.prex.base.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prex.base.api.entity.SysLog;

/**
 * <p>
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-27
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
