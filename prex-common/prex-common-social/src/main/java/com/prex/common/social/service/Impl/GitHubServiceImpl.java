package com.prex.common.social.service.Impl;

import com.prex.common.social.config.GitHubConfig;
import com.prex.common.social.config.SocialRestTemplate;
import com.prex.common.social.entity.GitHubUserInfo;
import com.prex.common.auth.exception.SocialServiceException;
import com.prex.common.social.service.GitHubService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * @Classname GitHubServiceImpl
 * @Description TODO
 * @Author Created by Lihaodong (alias:小东啊) im.lihaodong@gmail.com
 * @Date 2019-09-16 18:10
 * @Version 1.0
 */
@Slf4j
@Service
public class GitHubServiceImpl implements GitHubService {

    @Autowired
    private GitHubConfig gitHubConfig;

    private static final String URL_GET_USRE_INFO = "https://api.github.com/user";
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z", Locale.ENGLISH);

    @Override
    public GitHubUserInfo getUserInfo(String code) {

        try {
            // 自己拼接url
            String clientId = gitHubConfig.getAppId();
            String clientSecret = gitHubConfig.getAppSecret();
            String url = String.format("https://github.com/login/oauth/access_token?client_id=%s&client_secret=%s&code=%s", clientId, clientSecret, code);
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            URI uri = builder.build().encode().toUri();
            String responseStr = SocialRestTemplate.getRestTemplate().getForObject(uri, String.class);
            String[] items = StringUtils.splitByWholeSeparatorPreserveAllTokens(responseStr, "&");
            String accessToken = StringUtils.substringAfterLast(items[0], "=");
            log.info("获取Toke的响应：" + accessToken);

            Map<String, ?> user = SocialRestTemplate.getRestTemplate().getForObject(URL_GET_USRE_INFO, Map.class);
            Long gitHubId = Long.valueOf(String.valueOf(user.get("id")));
            String username = String.valueOf(user.get("login"));
            String name = String.valueOf(user.get("name"));
            String location = user.get("location") != null ? String.valueOf(user.get("location")) : null;
            String company = user.get("company") != null ? String.valueOf(user.get("company")) : null;
            String blog = user.get("blog") != null ? String.valueOf(user.get("blog")) : null;
            String email = user.get("email") != null ? String.valueOf(user.get("email")) : null;
            Date createdDate = toDate(String.valueOf(user.get("created_at")), dateFormat);
            String gravatarId = (String) user.get("gravatar_id");
            String profileImageUrl = gravatarId != null ? "https://secure.gravatar.com/avatar/" + gravatarId : null;
            String avatarUrl = user.get("avatar_url") != null ? String.valueOf(user.get("avatar_url")) : null;
            GitHubUserInfo userInfo = GitHubUserInfo.builder()
                    .id(gitHubId)
                    .username(username)
                    .name(name)
                    .location(location)
                    .company(company)
                    .blog(blog)
                    .email(email)
                    .profileImageUrl(profileImageUrl)
                    .avatarUrl(avatarUrl)
                    .createdDate(createdDate)
                    .build();
            log.info("github userInfo : [{}]", userInfo);
            return userInfo;
        } catch (Exception e) {
            log.error("GitHub登录信息错误,{}",e.getLocalizedMessage());
        }
        throw new SocialServiceException("GitHub登录信息错误",null);
    }

    private Date toDate(String dateString, DateFormat dateFormat) {
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }
}
